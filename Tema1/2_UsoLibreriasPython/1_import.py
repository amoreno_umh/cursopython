import pickle

clientes = []
numCuenta = 0

cuenta={'nombre': 'Javier', 'apellido': 'Ceballos', 'cuenta': {'saldo': 0, 'numeroCuenta': numCuenta}}

clientes.append(cuenta)

print('Cliente a guardar en el fichero')
print('Nombre: ' + clientes[0]['nombre'])
print('Apellido: ' + clientes[0]['apellido'])
print('Número de Cuenta: ' + str(clientes[0]['cuenta']['numeroCuenta']))
with open('lista.txt', 'wb') as f:
    pickle.dump(clientes, f)

with open('lista.txt', 'rb') as f:
    prueba = pickle.load(f)

print('\nCliente recuperado del fichero')
print('Nombre: ' + prueba[0]['nombre'])
print('Apellido: ' + prueba[0]['apellido'])
print('Número de Cuenta: ' + str(prueba[0]['cuenta']['numeroCuenta']))