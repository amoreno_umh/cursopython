Clientes = []
numCuentas = 0


def crearCuenta(clientes, numcuentas):
    # Crear cuenta bancaria
    nombre = input('Introduzca nombre: ')
    apellido = input('Introduzca apellido: ')
    cuenta = {'nombre': nombre, 'apellido': apellido, 'cuenta': {'saldo': 0, 'numeroCuenta': numCuentas}}
    clientes.append(cuenta)
    numcuentas += 1
    print('Cuenta creada')

    input('Pulse enter para continuar...')


def hacerIngreso(clientes):
    num_elem = len(clientes)

    if num_elem > 0:
        cuenta = input('Por favor indique el número de cuenta al que desea hacer el ingreso:')
        cantidad = input('Indique la cantidad a ingresar: ')
        saldoActual = clientes[int(cuenta)]['cuenta']['saldo']
        clientes[int(cuenta)]['cuenta']['saldo'] = saldoActual + int(cantidad)
        print('Se ha realizado el ingreso')
    else:
        print('No existe la cuenta')

    input('Pulse enter para continuar...')


def visualizarCuentas(clientes):
    if len(clientes):
        for cliente in clientes:
            print('Nombre: ' + cliente['nombre'])
            print('Apellido: ' + cliente['apellido'])
            print('Número de Cuenta: ' + str(cliente['cuenta']['numeroCuenta']))
            print('Saldo: ' + str(cliente['cuenta']['saldo']))
            print('\n')
    else:
        print('No existen cuentas para listar')

    input('Pulsa enter para continuar...')


def verSaldo(clientes):
    if len(clientes) > 0:
        cuenta = input('Indique número de cuenta')
        print('Saldo cuenta ' + int(cuenta) + ' es de ' + int(clientes[int(cuenta)]['cuenta']['saldo']) + ' Euros')
    else:
        print('no existen cuentas')

    input('Pulse enter para continuar...')


def hacerRetirada(clientes):
    if len(clientes) > 0:
        cuenta = input('Indique número de cuenta')
        cantidad = input('Indique cantidad a retirar')
        saldoActual = clientes[int(cuenta)]['cuenta']['saldo']
        clientes[int(cuenta)]['cuenta']['saldo'] = saldoActual - int(cantidad)
        print('Se ha realizado la retirada')
    else:
        print('no existen cuentas')

    input('Pulse enter para continuar')


def mostrarMenu():
    # Inicio
    opcion = '0'

    while (opcion != '6'):
        opcion = input('''Por favor seleccione una opción:
        1 Ver cuentas
        2 Crear cuenta
        3 Ver saldo
        4 Hacer ingreso
        5 Hacer retirada
        6 Salir
        ''')

        if opcion == '1':
            visualizarCuentas(Clientes)
        elif opcion == '2':
            crearCuenta(Clientes, numCuentas)
        elif opcion == '3':
            verSaldo(Clientes)
        elif opcion == '4':
            hacerIngreso(Clientes)
        elif opcion == '5':
            hacerRetirada(Clientes)

    print('Fin del programa')


mostrarMenu()
