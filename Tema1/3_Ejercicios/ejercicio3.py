import pickle

lista = ['trigo', 'avena', 'maiz', 'arroz']

#Guardo la lista desordenada
with open('ejercicio3_lista.txt', 'wb') as f:
    pickle.dump(lista, f)

#Abro la lista desordenada
with open('ejercicio3_lista.txt', 'rb') as f:
    lista_fichero = pickle.load(f)

lista_fichero.sort()
print(lista_fichero)

#Guardo la lista ordenada
with open('ejercicio3_lista_ordenada.txt', 'wb') as f:
    pickle.dump(lista_fichero, f)
