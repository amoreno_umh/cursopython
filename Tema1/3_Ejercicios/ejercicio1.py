# Programa calculadora
# Declaración de variables
opcion = 0


# Declaración de Funciones
def suma(a, b):
    return str(a + b)


def resta(a, b):
    return str(a - b)


def multiplicar(a, b):
    return str(a * b)


def dividir(a, b):
    return str(a / b)


# Inicio del programa

while ('5' != opcion):
    opcion = input('''Por favor seleccione una operacion:
     1 Sumar
     2 Restar
     3 Multiplicar
     4 Dividir
     5 Salir
     ''')

    if (opcion == '1'):
        Dato1 = input('''Primer sumando: ''')
        Dato2 = input('''Segundo sumando: ''')
        print("Resulta de " + Dato1 + " + " + Dato2 + ": " + suma(int(Dato1), int(Dato2)))
    elif opcion == '2':
        Dato1 = input('''Minuendo: ''')
        Dato2 = input('''Sustraendo: ''')
        suma(Dato1, Dato2)
        print("Resulta de " + Dato1 + " - " + Dato2 + ": " + resta(int(Dato1), int(Dato2)))
    elif opcion == '3':
        Dato1 = input('''Multiplicando: ''')
        Dato2 = input('''Multiplicador: ''')
        suma(Dato1, Dato2)
        print("Resulta de " + Dato1 + " * " + Dato2 + ": " + multiplicar(int(Dato1), int(Dato2)))
    elif opcion == '4':
        Dato1 = input('''Dividendo: ''')
        Dato2 = input('''Divisor: ''')
        suma(Dato1, Dato2)
        print("Resulta de " + Dato1 + " / " + Dato2 + ": " + dividir(float(Dato1), float(Dato2)))
    elif opcion == '0' or opcion > '5':
        print("Opcion incorrecta")
    if (opcion != '5'):
        input("Pulse Enter para continuar...")

print('Fin del programa')
