from cuentaEspecial import CuentaAhorro
from cuentaEspecial import CuentaCorriente
import time
import pickle

numCuentas = 0
clientes = []
opcion = 0

def abrirCuenta(numCuentas):
    opcion = input('''Por favor seleccione un tipo de cuenta:
        1 Cuenta Corriente
        2 Cuenta de Ahorro
        3 Atras
        ''')
    if opcion == '1':

        nombre = input('''Nombre: ''')
        apellidos = input('''Apellidos: ''')
        cantidad= input('''Ingreso inicial: ''')
        tipoInteres= input('''Tipo de interes: ''')
        tarjetaDeb = input('''Tarjeta de Debito(1-Si,0-No): ''')
        if tarjetaDeb==1:
            tarjetaDebito=True
        else:
            tarjetaDebito=False
        tarjetaCre = input('''Tarjeta de Credito(1-Si,0-No): ''')
        if tarjetaCre == 1:
            tarjetaCredito = True
        else:
            tarjetaCredito = False

        cuotaMantenimiento=input('''Cuota de Mantenimiento: ''')
        cuentaAux = CuentaCorriente(nombre, apellidos, numCuentas, float(cantidad), float(tipoInteres),
                                     tarjetaDebito, tarjetaCredito, float(cuotaMantenimiento))

        return cuentaAux

    elif opcion == '2':
        nombre = input('''Nombre: ''')
        apellidos = input('''Apellidos: ''')
        cantidad = input('''Ingreso inicial: ''')
        tipoInteres = input('''Tipo de interes: ''')
        fecha=time.strftime("%d/%m/%y")
        cuentaAux = CuentaAhorro(nombre,apellidos,numCuentas, float(cantidad), fecha, float(tipoInteres))
        return cuentaAux
    elif opcion == '3':
        return
try:
    with open('cuentas.dat', 'rb') as f:
        clientes = pickle.load(f)
except:
    f = open('cuentas.dat', 'wb')
    f.close()

while ('6' != opcion):
    opcion = input('''Por favor seleccione una operacion:
    1 Abrir una cuenta
    2 Ver cuentas
    3 Ver saldo
    4 Hacer ingreso
    5 Hacer retirada
    6 Exit
    ''')

    if opcion == '1':
        clienteAux=abrirCuenta(numCuentas)
        if clienteAux is not None:
            clientes.append(clienteAux)
            numCuentas += 1

    elif opcion == '2':

        for cliente in clientes:
            print('Nombre: ' + cliente.getNombre())
            print('Apellido: ' + cliente.getApellido())
            print('Numero de Cuenta: ' + str(cliente.getNumeroCuenta()))
            print('\n')
        input("Pulse Enter para continuar...")
    elif opcion == '3':
        cuenta = input('Por favor indique el numero de cuenta:')
        print('El saldo de la cuenta ' +cuenta+' es de: '+str(clientes[int(cuenta)].getSaldo())+' Euros')
        input("Pulse Enter para continuar...")
        
    elif opcion == '4':
        cuenta = input('Por favor indique el numero de cuenta al que desea hacer el ingreso:')
        cantidad = input('Por favor indique la cantidad a ingresar:')
        clientes[int(cuenta)].ingresarDinero(int(cantidad))
        print('Se ha realizado el ingreso')
        input("Pulse Enter para continuar...")
        
    elif opcion == '5':
        cuenta = input('Por favor indique el numero de cuenta al que desea hacer una retirada:')
        cantidad = input('Por favor indique la cantidad a retirar:')
        clientes[int(cuenta)].sacarDinero(float (cantidad))
        print('Se ha realizado la retirada')
        input("Pulse Enter para continuar...")


with open('cuentas.dat', 'wb') as f:
    pickle.dump(clientes, f)

print('Fin del programa')
