from cuentaEspecial import CuentaAhorro
from cuentaEspecial import CuentaCorriente
from datos import Datos
import time

datos = Datos('cuentas.dat')
opcion = 0

def abrirCuenta(numCuenta):

    opcion = input('''Por favor seleccione un tipo de cuenta:
        1 Cuenta Corriente
        2 Cuenta de Ahorro
        3 Atras
        ''')
    if opcion == '1':

        nombre = input('''Nombre: ''')
        apellidos = input('''Apellidos: ''')
        cantidad= input('''Ingreso inicial: ''')
        tipoInteres= input('''Tipo de interes: ''')
        tarjetaDeb = input('''Tarjeta de Debito(1-Si,0-No): ''')
        if tarjetaDeb==1:
            tarjetaDebito=True
        else:
            tarjetaDebito=False
        tarjetaCre = input('''Tarjeta de Credito(1-Si,0-No): ''')
        if tarjetaCre == 1:
            tarjetaCredito = True
        else:
            tarjetaCredito = False

        cuotaMantenimiento=input('''Cuota de Mantenimiento: ''')
        cuentaAux = CuentaCorriente(nombre, apellidos, numCuenta, float(cantidad), float(tipoInteres),
                                     tarjetaDebito, tarjetaCredito, float(cuotaMantenimiento))

        return cuentaAux

    elif opcion == '2':
        nombre = input('''Nombre: ''')
        apellidos = input('''Apellidos: ''')
        cantidad = input('''Ingreso inicial: ''')
        tipoInteres = input('''Tipo de interes: ''')
        fecha=time.strftime("%d/%m/%y")
        cuentaAux = CuentaAhorro(nombre,apellidos,numCuenta, float(cantidad), fecha, float(tipoInteres))
        return cuentaAux
    elif opcion == '3':
        return


while ('6' != opcion):
    opcion = input('''Por favor seleccione una operacion:
    1 Abrir una cuenta
    2 Ver cuentas
    3 Ver saldo
    4 Hacer ingreso
    5 Hacer retirada
    6 Exit
    ''')

    datos.load()

    if opcion == '1':
        numCuenta = len(datos.clientes)
        clienteAux= abrirCuenta(numCuenta)
        if clienteAux is not None:
            datos.clientes.append(clienteAux)

        datos.save()

    elif opcion == '2':

        for cliente in datos.clientes:
            print('Nombre: ' + cliente.getNombre())
            print('Apellido: ' + cliente.getApellido())
            print('Numero de Cuenta: ' + str(cliente.getNumeroCuenta()))
            print('Saldo: ' + str(cliente.getSaldo()))
            print('\n')
        input("Pulse Enter para continuar...")
    elif opcion == '3':
        cuenta = input('Por favor indique el numero de cuenta:')
        cliente = [cliente for cliente in datos.clientes if cliente.getNumeroCuenta() == int(cuenta)][0]
        print('Titular:', cliente.getNombre(), cliente.getApellido())
        print('El saldo de la cuenta ' +cuenta+' es de: '+str(cliente.getSaldo())+' Euros')
        input("Pulse Enter para continuar...")
        
    elif opcion == '4':
        cuenta = input('Por favor indique el numero de cuenta al que desea hacer el ingreso:')
        cantidad = input('Por favor indique la cantidad a ingresar:')
        cliente = [cliente for cliente in datos.clientes if cliente.getNumeroCuenta() == int(cuenta)]
        cliente[0].ingresarDinero(int(cantidad))
        datos.save()
        print('Se ha realizado el ingreso')
        input("Pulse Enter para continuar...")
        
    elif opcion == '5':
        cuenta = input('Por favor indique el numero de cuenta al que desea hacer una retirada:')
        cantidad = input('Por favor indique la cantidad a retirar:')
        cliente = [cliente for cliente in datos.clientes if cliente.getNumeroCuenta() == int(cuenta)]
        cliente[0].sacarDinero(float (cantidad))
        datos.save()
        print('Se ha realizado la retirada')
        input("Pulse Enter para continuar...")

print('Fin del programa')
