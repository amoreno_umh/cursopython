from math import pi


class Figura(object):
    def area(self):
        pass


class Circulo(Figura):
    def __init__(self, radio=0):
        self.radio = radio

    def area(self):
        return pi * self.radio * self.radio


class Triangulo(Figura):
    def __init__(self, base=0, altura=0):
        self.base = base
        self.altura = altura

    def area(self):
        return self.base * self.altura / 2.